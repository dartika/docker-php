FROM php:7.2-fpm-alpine

# install commonly used php extensions
RUN apk add --no-cache \
    freetype-dev libpng-dev libjpeg-turbo-dev freetype libpng libjpeg-turbo && \
    docker-php-ext-configure gd \
    --with-gd \
    --with-freetype-dir=/usr/include/ \
    --with-png-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ && \
    NPROC=$(grep -c ^processor /proc/cpuinfo 2>/dev/null || 1) &&  \
    docker-php-ext-install -j${NPROC} gd pdo pdo_mysql opcache zip bcmath &&  \
    apk del --no-cache freetype-dev libpng-dev libjpeg-turbo-dev

# install composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# php config
ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS=1

COPY conf.d/*.ini /usr/local/etc/php/conf.d/
COPY php.ini /usr/local/etc/php/php.ini
